import './App.css';
import Add from './components/add';
import { StudentInfoContext } from './Helper/Context';
import { useState } from 'react';
import Student from './components/studentinfo';
// import Display from './components/display';
import Searchfilter from './components/searchfilter';
import Title from './components/title';

function App() {

  const [newstudents, setNewStudents] = useState(Student);

  return (
    // put components inside to access newstudents.
    <StudentInfoContext.Provider value={{ newstudents, setNewStudents }}>
      <Title />
      <Add />
      {/* <Update/> */}
      {/* <Display /> */}
      <Searchfilter />
    </StudentInfoContext.Provider>
  );
}

export default App;
