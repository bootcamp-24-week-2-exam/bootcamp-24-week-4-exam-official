import { createContext } from "react";

export const StudentInfoContext = createContext(null);