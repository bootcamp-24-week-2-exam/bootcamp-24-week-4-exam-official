const Title = () => {
    return (
        <h1 className="title-style">
            Student Information System
        </h1>
    );
}

export default Title;