import {Button, Modal, Form} from 'react-bootstrap';
import React, {useState, useContext} from 'react';
import { StudentInfoContext } from '../Helper/Context';


// add
function Add() {

    // global student information context.
    const {newstudents, setNewStudents} = useContext(StudentInfoContext);

    // for modals
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    // for inputs
    const [firstname, setfirstName] = useState("");
    const [lastname, setlastName] = useState("");
    const [middlename, setmiddleName] = useState("");
    const [address, setAddress] = useState("");
    const [age, setAge] = useState("");
    const [zip, setZIP] = useState("");
    const [city, setCity] = useState("");
    const [regionprovince, setRegionProvince] = useState("");
    const [phone, setPhone] = useState("");
    const [mobile, setMobile] = useState("");
    const [email, setEmail] = useState("");
    const [yrlevel, setYrLevel] = useState("");
    const [section, setSection] = useState("");

    // functions for onchange.
    const handleFirstname = (e) => {
        setfirstName(e.target.value);
        // console.log(e.target.value);
    }
    const handleLastname = (e) => {
        setlastName(e.target.value);
        // console.log(e.target.value);
    }
    const handleMiddlename = (e) => {
        setmiddleName(e.target.value);
        // console.log(e.target.value);
    }
    const handleAddress = (e) => {
        setAddress(e.target.value);
        // console.log(e.target.value);
    }
    const handleAge= (e) => {
        setAge(e.target.value);
        // console.log(e.target.value);
    }
    const handleZIP = (e) => {
        setZIP(e.target.value);
        // console.log(e.target.value);
    }
    const handleCity = (e) => {
        setCity(e.target.value);
        // console.log(e.target.value);
    }
    const handleRegionProvince = (e) => {
        setRegionProvince(e.target.value);
        // console.log(e.target.value);
    }
    const handlePhone = (e) => {
        setPhone(e.target.value);
        // console.log(e.target.value);
    }
    const handleMobile = (e) => {
        setMobile(e.target.value);
        // console.log(e.target.value);
    }
    const handleEmail = (e) => {
        setEmail(e.target.value);
        // console.log(e.target.value);
    }
    const handleYrlevel = (e) => {
        setYrLevel(e.target.value);
        // console.log(e.target.value);
    }
    const handleSection = (e) => {
        setSection(e.target.value);
        // console.log(e.target.value);
    }


    // handle submt button.
    const handleSubmitBtn = (e) => {
        if((firstname === "" || lastname === "" || middlename === "" || address === "" || age === "" || zip === "" || city === "" || regionprovince === "" || phone === "" || mobile === "" || email === "" || yrlevel === "" || section === "") && e === true) {
            return true;
        } else {
            return false;
        }
    }

    // handle the submit.
    const handleSubmit = (e) => {
        e.preventDefault();
        // get curretn timestamp.
        let date;
        date = new Date();
        date = date.getUTCFullYear() + '-' +
            ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
            ('00' + date.getUTCDate()).slice(-2) + ' ' + 
            ('00' + date.getUTCHours()).slice(-2) + ':' + 
            ('00' + date.getUTCMinutes()).slice(-2) + ':' + 
            ('00' + date.getUTCSeconds()).slice(-2);

        // get last student id.
        let rowLen = newstudents.length;
        let lastRow;
        newstudents.map((student, i) => {
            if(rowLen === i + 1) {
                lastRow = student.id;
            }
            return lastRow;
        })


        let newStudent = [
        {
            id: lastRow + 1,
            stud_firstname: firstname,
            stud_lastname: lastname,
            stud_middlename: middlename,
            stud_address: address,
            stud_age: age,
            stud_zip: zip,
            stud_city: city,
            stud_regionProvince: regionprovince,
            stud_phoneNo: phone,
            stud_mobileNo: mobile,
            stud_email: email,
            stud_yrlvl: yrlevel,
            stud_section: section,
            sec_UpdatedAt: date
        }];

        let Student2 = [...newstudents, ...newStudent];
        setNewStudents(Student2);
        console.log(Student2);

    }

    // pass NewStudent to function.

    return (
        <div>
            {/* for adding student. */}
            <Button className="add-btn" variant="primary" onClick={handleShow}>Add Student</Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>Add Student</Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3" controlId="formFirstname">
                        <Form.Label>First name</Form.Label>
                        <Form.Control name="fname" type="text" placeholder="Enter first name" required value={firstname} onChange={handleFirstname} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formLastname" >
                        <Form.Label>Last name</Form.Label>
                        <Form.Control name="lname" type="text" placeholder="Enter last name" required value={lastname} onChange={handleLastname}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formMiddlename">
                        <Form.Label>Middle name</Form.Label>
                        <Form.Control name="mname" type="text" placeholder="Enter middle name" required value={middlename} onChange={handleMiddlename}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formAddress">
                        <Form.Label>Address</Form.Label>
                        <Form.Control name="address" type="text" placeholder="Address" required value={address} onChange={handleAddress} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formAge">
                        <Form.Label>Age</Form.Label>
                        <Form.Control name="age" type="text" placeholder="Age" required value={age} onChange={handleAge}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formZIP">
                        <Form.Label>ZIP</Form.Label>
                        <Form.Control name="zip" type="text" placeholder="ZIP code" required value={zip} onChange={handleZIP}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control name="city" type="text" placeholder="City" required value={city} onChange={handleCity} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formRegionProvince">
                        <Form.Label>Region or Province</Form.Label>
                        <Form.Control name="regionprovince" type="text" placeholder="Region/Province" required value={regionprovince} onChange={handleRegionProvince} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formPhone">
                        <Form.Label>Phone no.</Form.Label>
                        <Form.Control name="phone" type="text" placeholder="Phone no." required value={phone} onChange={handlePhone}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formMobile">
                        <Form.Label>Mobile no.</Form.Label>
                        <Form.Control name="mobile" type="text" placeholder="Mobile no." required value={mobile} onChange={handleMobile} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control name="email" type="email" placeholder="Email" required value={email} onChange={handleEmail} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formYrLevel">
                        <Form.Label>Year Level</Form.Label>
                        <Form.Select name="yrlevel" required  value={yrlevel} onChange={handleYrlevel}aria-label="Default select example">
                        <option>Year Level...</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formSection">
                        <Form.Label>Section</Form.Label>
                        <Form.Select name="section" required value={section} onChange={handleSection}aria-label="Default select example">
                        <option>Section...</option>
                        <option value="1">Saint Francis</option>
                        <option value="2">Saint Paul</option>
                        <option value="3">Saint Therese</option>
                        <option value="4">Saint John</option>
                        <option value="5">Saint Peter</option>
                        <option value="6">Saint Luke</option>
                        </Form.Select>
                    </Form.Group>
                    <Modal.Footer>
                    <Button variant="secondary" type="submit" onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button variant="primary" type="submit" disabled={handleSubmitBtn(true)} onClick={handleClose}>
                        Submit
                    </Button>
                    </Modal.Footer>
                    </Form>
                </Modal.Body>
            </Modal>
        </div>
  );

  }


  export default Add;