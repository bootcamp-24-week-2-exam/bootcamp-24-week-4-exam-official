import Sections from './sectioninfo'
import {Button, Modal, Form, Table} from 'react-bootstrap';

const DataTable = ({data}, {onSubmit}) => {
    console.log('DATAS:', Sections);
  
    // data = data.map((data, Sections) => {
    //     console.log('ss:', Sections.map(({section_id, sec_section}) => sec_section));
    //     if(data.stud_section === parseInt(Sections.section_id)){
    //         return {
    //             ...data,
    //             stud_section: Sections.sec_section,
    //         };
    //     }
    //     return {
    //         ...data,
    //     };
    // });
    
    // console.log('DATASs:', data);
    // console.log('DATASs:', [...data, [{stud_section : 'SAM'}]]);
	return (
        <Form onSubmit={onSubmit}>
         <Table striped bordered hover size="sm" responsive="sm">
            <thead>
                <tr>
                    {Object.keys(data[0]).map((key) => {
                        if(key === 'stud_yrlvl'){
                            key = 'Year Level'
                        }else if(key === 'stud_section'){
                            key = 'Section'
                        }else if(key === 'stud_city'){
                            key = 'City'
                        }else if(key === 'stud_zip'){
                            key = 'Zip'
                        }else if(key === 'stud_age'){
                            key = 'Age'
                        }
                    return <th key={key}>{key}</th>
                    })}
                </tr>
            </thead>
            <tbody>
                {data.map((item) => (
                <tr key={item.id}>
                {Object.values(item).map((val) => (
                    <td key={val}>{val}</td>
                ))}
                 </tr>
                 ))}
            </tbody>
        </Table>
        </Form>
     
	);
};

export default DataTable;