import { useState, useContext } from 'react';
import Student from "./studentinfo";
import FilterBy from "./filters";
import DataTable from './table';
import {Button, Form, Modal} from 'react-bootstrap';
import { StudentInfoContext } from '../Helper/Context';
import ModalView from './showModal';
import Display from './display';

const Searchfilter = () => {
    const {newstudents, setNewStudents} = useContext(StudentInfoContext);


    const [selectedFilter, setSelectedFilter] = useState('');
    const [selectedData, setSelectedData] = useState({});
    const [showing, setShowing] = useState(false);
    const [showingSearch2, setShowingSearch2] = useState(false);
    const [search, setSearch] = useState('');
    const [search2, setSearch2] = useState('');
    const [placeHolder, setPlaceHolder] = useState('');
    const [textFieldEnable, setTextFieldEnable] = useState(true);
    const [searchBtnEnable, setSearchBtnEnable] = useState(true);
    const [minMax, setMinMax] = useState(false);

    const [showSearchTable, setShowSearchTable] = useState(false);

	const handleFilterSelect = (e) => {
        setTextFieldEnable(false);
        setSearchBtnEnable(true);
        setSearch('');
        setSearch2('');
        setSelectedFilter(e.target.value);
        switch(e.target.value){
            case '1':
                setShowingSearch2(true);
                setPlaceHolder('Year level')
                break;
            case'2':
                setShowingSearch2(false);
                setPlaceHolder('Section')
                break;
            case'3':
                setShowingSearch2(false);
                setPlaceHolder('City')
                break;
            case'4':
                setShowingSearch2(true);
                setPlaceHolder('Zip')
                break;
            case'5':
                setShowing(false);
                setShowingSearch2(true);
                setPlaceHolder('Minimum age')
                break;
            case'6':
                setShowing(false);
                setShowingSearch2(false);
                setPlaceHolder('ID/Lastname/firstname/')
                
                break;
            default:
                break;
        }
    };

    const handleClick = (e) => {
        //e.preventDefault();
        if(selectedFilter==='6' || selectedFilter==='5'){
            setShowSearchTable(true);
        }else{
            setShowSearchTable(false);
            setShowing(false);
            if(getResult(selectedFilter, search, search2, newstudents).length > 0 || !minMax){
                setSelectedData(getResult(selectedFilter, search, search2, newstudents))
                setShowing(true);
            }
        }
      };

    const handleChange = (e) => {
		setSearch(showingSearch2 ? e.target.value.replace(/\D/g, '') : e.target.value);
        setSearchBtnEnable(e.target.value !== '' ? false : true);
        if(selectedFilter === '5')
            setMinMax(minmaxEnable(e.target.value, search2));
	};

    const handleChange2 = (e) => {
		setSearch2(showingSearch2 ? e.target.value.replace(/\D/g, '') : e.target.value);
        setSearchBtnEnable(e.target.value !== '' ? false : true);
        if(selectedFilter === '5')
            setMinMax(minmaxEnable(search, e.target.value));
	};

    

    const handleClickSample = (e) => {
		
        console.log('MODAL');
	};

	return (
        <main className='filterform-style'>
            <label className='filterby-style'>Filter by :  
                <Form.Select className="filter-style" value={selectedFilter} onChange={handleFilterSelect}>
                    <option value='' disabled>
                    Select
                    </option>
                    {FilterBy.map(({id, tag}) => (
                    <option key={id} value={id}>{tag}</option>
                    ))}
                </Form.Select>
                <Form.Control id="search" type="text" disabled={textFieldEnable} value={search} placeholder={placeHolder} onChange={handleChange}/>
                {showingSearch2 && selectedFilter === '5' && <Form.Control id="search2" type="text" value={search2} placeholder='Maximum Age' onChange={handleChange2}/>}
                <Button className='search-btn' variant="primary" type='submit' disabled={searchBtnEnable} onClick={ handleClick }>
			    Search
		        </Button>
            </label>
            {minMax && <label>Error: {search} is greater than {search2}</label>}

            {showing && selectedData && 
                <DataTable
				data={selectedData}
                onSubmit={handleClick}
                />
            }

            {showSearchTable && <Display search={search} search2={search2}></Display>}
        </main>
	);
};

const getResult = (filterby, search, search2, newstudents) => {
    let result;
    switch(filterby) {
        case '1':
            result = newstudents.filter(
                ({stud_yrlvl}) => stud_yrlvl === parseInt(search)
                ).map(
                    ({id, stud_lastname, stud_firstname, stud_middlename, stud_yrlvl}) => ({id, name: stud_lastname + ', ' + stud_firstname + ' ' + stud_middlename, stud_yrlvl})
                ).sort((a, b) => (a.name > b.name) ? 1 : -1);
            break;
        case '2':
            result = newstudents.filter(
                ({stud_section}) => stud_section === parseInt(search)
            ).map(
                ({id, stud_lastname, stud_firstname, stud_middlename, stud_section}) => ({id, name: stud_lastname + ', ' + stud_firstname + ' ' + stud_middlename, stud_section})
            ).sort((a, b) => (a.name > b.name) ? 1 : -1);
            break;
        case '3':
            result = newstudents.filter(
                ({stud_city}) => stud_city.toLowerCase().includes(search.toLowerCase())
            ).map(
                ({id, stud_lastname, stud_firstname, stud_middlename, stud_city}) => ({id, name: stud_lastname + ', ' + stud_firstname + ' ' + stud_middlename, stud_city})
            ).sort((a, b) => (a.name > b.name) ? 1 : -1);              
            break;
        case '4':
            result = newstudents.filter(
                ({stud_zip}) => stud_zip === parseInt(search)
            ).map(
                ({id, stud_lastname, stud_firstname, stud_middlename, stud_zip}) => ({id, name: stud_lastname + ', ' + stud_firstname + ' ' + stud_middlename, stud_zip})
            ).sort((a, b) => (a.name > b.name) ? 1 : -1);                            
            break;
        case '5':
            result = newstudents.filter(
                ({stud_age}) => stud_age >= parseInt(search) && stud_age <= parseInt(search2)
                ).map(
                    ({id, stud_lastname, stud_firstname, stud_middlename, stud_age}) => ({id, name: stud_lastname + ', ' + stud_firstname + ' ' + stud_middlename, stud_age})
                ).sort((a, b) => (a.name > b.name) ? 1 : -1);                         
            break;
        default:
            console.log('none selected')
            break;

    }
    return result;
};

const minmaxEnable = (search, search2) => {
    if(search2 !== '' || search2 !== null){
        if(parseInt(search) > parseInt(search2)){
            console.log('Error: ', search, 'is greater than ', search2);
            return true;
        }else{
            return false;
        }
    }
};

// const Modalview = (data) => {
//     // for modals
//     const [show, setShow] = useState(false);
//     const handleClose = () => setShow(false);
//     const handleShow = () => setShow(true);

//     return (
//         <div>
//             {/* for adding student. */}
//             <Button variant="primary" onClick={handleShow}>Add Student</Button>
//             <Modal show={show} onHide={handleClose}>
//                 <Modal.Header closeButton>Add Student</Modal.Header>
//                 <Modal.Body>
//                     <Form >
//                     <Form.Group className="mb-3" controlId="formFirstname">
//                         <Form.Label>First name</Form.Label>
//                         <Form.Control name="fname" type="text" placeholder="Enter first name" required value={data.stud_firstname} />
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formLastname" >
//                         <Form.Label>Last name</Form.Label>
//                         <Form.Control name="lname" type="text" placeholder="Enter last name" required value={data.stud_lastname}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formMiddlename">
//                         <Form.Label>Middle name</Form.Label>
//                         <Form.Control name="mname" type="text" placeholder="Enter middle name" required value={data.stud_middlename}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formAddress">
//                         <Form.Label>Address</Form.Label>
//                         <Form.Control name="address" type="text" placeholder="Address" required value={data.stud_address} />
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formAge">
//                         <Form.Label>Age</Form.Label>
//                         <Form.Control name="age" type="text" placeholder="Age" required value={data.stud_age}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formZIP">
//                         <Form.Label>ZIP</Form.Label>
//                         <Form.Control name="zip" type="text" placeholder="ZIP code" required value={data.stud_zip}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formCity">
//                         <Form.Label>City</Form.Label>
//                         <Form.Control name="city" type="text" placeholder="City" required value={data.stud_city} />
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formRegionProvince">
//                         <Form.Label>Region or Province</Form.Label>
//                         <Form.Control name="regionprovince" type="text" placeholder="Region/Province" required value={data.stud_regionprovince} />
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formPhone">
//                         <Form.Label>Phone no.</Form.Label>
//                         <Form.Control name="phone" type="text" placeholder="Phone no." required value={data.stud_phone}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formMobile">
//                         <Form.Label>Mobile no.</Form.Label>
//                         <Form.Control name="mobile" type="text" placeholder="Mobile no." required value={data.stud_mobile}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formEmail">
//                         <Form.Label>Email</Form.Label>
//                         <Form.Control name="email" type="email" placeholder="Email" required value={data.stud_email}/>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formYrLevel">
//                         <Form.Label>Year Level</Form.Label>
//                         <Form.Select name="yrlevel" required  value={data.stud_yrlevel}aria-label="Default select example">
//                         <option>Year Level...</option>
//                         <option value="1">1</option>
//                         <option value="2">2</option>
//                         <option value="3">3</option>
//                         <option value="4">4</option>
//                         </Form.Select>
//                     </Form.Group>
//                     <Form.Group className="mb-3" controlId="formSection">
//                         <Form.Label>Section</Form.Label>
//                         <Form.Select name="section" required value={data.stud_section}aria-label="Default select example">
//                         <option>Section...</option>
//                         <option value="1">Saint Francis</option>
//                         <option value="2">Saint Paul</option>
//                         <option value="3">Saint Therese</option>
//                         <option value="4">Saint John</option>
//                         <option value="5">Saint Peter</option>
//                         <option value="6">Saint Luke</option>
//                         </Form.Select>
//                     </Form.Group>
//                     <Modal.Footer>
//                     <Button variant="secondary" type="submit" onClick={handleClose}>
//                         Cancel
//                     </Button>
//                     {/* <Button variant="primary" type="submit" disabled={handleSubmitBtn(true)} onClick={handleClose}>
//                         Submit
//                     </Button> */}
//                     </Modal.Footer>
//                     </Form>
//                 </Modal.Body>
//             </Modal>
//         </div>
//   );
// };
  
  export default Searchfilter;