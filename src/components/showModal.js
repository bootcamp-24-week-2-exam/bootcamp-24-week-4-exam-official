import {Button, Modal, Form} from 'react-bootstrap';
import React, {useState, useContext} from 'react';
import { StudentInfoContext } from '../Helper/Context';


const ModalView = (data, showM) => {
    // for modals
    const [show, setShow] = useState(showM);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [getId, setGetId] = useState(data);

    const {newstudents, setNewStudents} = useContext(StudentInfoContext);

    console.log(newstudents);
    console.log(getId);
    //const getStudData = newstudents.filter(({id}) => parseInt(id) === parseInt(getId.data) );
    
    const getInfo = newstudents.filter(({id}) => parseInt(id) === parseInt(getId.data));
    
    console.log('gets', getInfo);

    return (
        <div>
            {console.log(getInfo.map(({id}) => id))}
            {/* for adding student. */}
            {/* <Button variant="primary" onClick={handleShow}>Add Student</Button> */}
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>Student Deatails</Modal.Header>
                <Modal.Body>
                    <Form >
                    <Form.Group className="mb-3" controlId="formFirstname">
                        <Form.Label>First name
                        </Form.Label>
                        <Form.Control name="fname" type="text" required value={getInfo.map(({stud_firstname}) => stud_firstname)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formLastname" >
                        <Form.Label>Last name</Form.Label>
                        <Form.Control name="lname" type="text" placeholder="Enter last name" required value={getInfo.map(({stud_lastname}) => stud_lastname)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formMiddlename">
                        <Form.Label>Middle name</Form.Label>
                        <Form.Control name="mname" type="text" placeholder="Enter middle name" required value={getInfo.map(({stud_middlename}) => stud_middlename)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formAddress">
                        <Form.Label>Address</Form.Label>
                        <Form.Control name="address" type="text" placeholder="Address" required value={getInfo.map(({stud_address}) => stud_address)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formAge">
                        <Form.Label>Age</Form.Label>
                        <Form.Control name="age" type="text" placeholder="Age" required value={getInfo.map(({stud_age}) => stud_age)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formZIP">
                        <Form.Label>ZIP</Form.Label>
                        <Form.Control name="zip" type="text" placeholder="ZIP code" required value={getInfo.map(({stud_zip}) => stud_zip)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control name="city" type="text" placeholder="City" required value={getInfo.map(({stud_city}) => stud_city)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formRegionProvince">
                        <Form.Label>Region or Province</Form.Label>
                        <Form.Control name="regionprovince" type="text" placeholder="Region/Province" required value={getInfo.map(({stud_regionProvince}) => stud_regionProvince)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formPhone">
                        <Form.Label>Phone no.</Form.Label>
                        <Form.Control name="phone" type="text" placeholder="Phone no." required value={getInfo.map(({stud_phoneNo}) => stud_phoneNo)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formMobile">
                        <Form.Label>Mobile no.</Form.Label>
                        <Form.Control name="mobile" type="text" placeholder="Mobile no." required value={getInfo.map(({stud_mobileNo}) => stud_mobileNo)} disabled/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control name="email" type="email" placeholder="Email" required value={getInfo.map(({stud_email}) => stud_email)} disabled/>
                    </Form.Group>
                    {/* <Form.Group className="mb-3" controlId="formYrLevel">
                        <Form.Label>Year Level</Form.Label>
                        <Form.Select name="yrlevel" required  value={data.stud_yrlevel}aria-label="Default select example">
                        <option>Year Level...</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formSection">
                        <Form.Label>Section</Form.Label>
                        <Form.Select name="section" required value={data.stud_section}aria-label="Default select example">
                        <option>Section...</option>
                        <option value="1">Saint Francis</option>
                        <option value="2">Saint Paul</option>
                        <option value="3">Saint Therese</option>
                        <option value="4">Saint John</option>
                        <option value="5">Saint Peter</option>
                        <option value="6">Saint Luke</option>
                        </Form.Select>
                    </Form.Group> */}
                    <Modal.Footer>
                    <Button variant="secondary" type="submit" onClick={handleClose}>
                        Cancel
                    </Button>
                    {/* <Button variant="primary" type="submit" disabled={handleSubmitBtn(true)} onClick={handleClose}>
                        Submit
                    </Button> */}
                    </Modal.Footer>
                    </Form>
                </Modal.Body>
            </Modal>
        </div>
  );
};

export default ModalView;