const FilterBy = [
	{id: 1, tag: 'Year Level'},
	{id: 2, tag: 'Section'},
	{id: 3, tag: 'City'},
	{id: 4, tag: 'Zip'},
    {id: 5, tag: 'Age Bracket'},
	{id: 6, tag: 'All'},
];

export default FilterBy;