import { useContext, useState, useEffect } from "react";
import { StudentInfoContext } from "../Helper/Context";
// import Student from "./studentinfo";
import {Table, Form, Button, Modal} from 'react-bootstrap';



function Display({search, search2}) {
    const {newstudents, setNewStudents} = useContext(StudentInfoContext);
    // const [newstudents2, setNewStudents2] = useState(data);
    const [show, setShow] = useState(false);

    const [updateView, setUpdateView] = useState(false);

    const handleHide = (event) => setShow(false);

    const [getInfo, setInfo] = useState([]);

    const [searchEn, setSearchEn] = useState(search !== null ? true : false);

        // for inputs
        const [firstname, setfirstName] = useState("");
        const [lastname, setlastName] = useState("");
        const [middlename, setmiddleName] = useState("");
        const [address, setAddress] = useState("");
        const [age, setAge] = useState("");
        const [zip, setZIP] = useState("");
        const [city, setCity] = useState("");
        const [regionprovince, setRegionProvince] = useState("");
        const [phone, setPhone] = useState("");
        const [mobile, setMobile] = useState("");
        const [email, setEmail] = useState("");
        const [yrlevel, setYrLevel] = useState("");
        const [section, setSection] = useState("");

    const handleClose = (event) => {
      //event.preventDefault();
      console.log('close');
      handleHide();
    };
    
    const handleShow = () => {
      setShow(true)
    };

    const handleClickModal = (event) => {
      event.preventDefault();
      setUpdateView(false);
      setInfo(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)));
      setfirstName(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_firstname}) => stud_firstname));
      setlastName(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_lastname}) => stud_lastname));
      setmiddleName(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_middlename}) => stud_middlename));
      setAddress(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_address}) => stud_address));
      setAge(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_age}) => stud_age));
      setZIP(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_zip}) => stud_zip));
      setCity(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_city}) => stud_city));
      setRegionProvince(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_regionProvince}) => stud_regionProvince));
      setPhone(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_phoneNo}) => stud_phoneNo));
      setMobile(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_mobileNo}) => stud_mobileNo));
      setEmail(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_email}) => stud_email));
      setYrLevel(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_yrlvl}) => stud_yrlvl));
      setSection(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_section}) => stud_section));
      handleShow();
    };

    const handleDelete = (event) => {
      event.preventDefault();
      setNewStudents(current =>
        current.filter(student => {
          // 👇️ remove object that has id equal to 2
          return student.id !== parseInt(event.target.value);
        }),
      );
    };

    const handleShowModalUpdate = (event) => {
      event.preventDefault();
      setUpdateView(true);
      setInfo(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)));
      setfirstName(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_firstname}) => stud_firstname));
      setlastName(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_lastname}) => stud_lastname));
      setmiddleName(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_middlename}) => stud_middlename));
      setAddress(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_address}) => stud_address));
      setAge(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_age}) => stud_age));
      setZIP(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_zip}) => stud_zip));
      setCity(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_city}) => stud_city));
      setRegionProvince(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_regionProvince}) => stud_regionProvince));
      setPhone(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_phoneNo}) => stud_phoneNo));
      setMobile(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_mobileNo}) => stud_mobileNo));
      setEmail(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_email}) => stud_email));
      setYrLevel(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_yrlvl}) => stud_yrlvl));
      setSection(newstudents.filter(({id}) => parseInt(id) === parseInt(event.target.value)).map(({stud_section}) => stud_section));
      handleShow();
    };

    const handleUpdate = (event) => {
      event.preventDefault();
      console.log('handleUpdate');
        let date = new Date();
        const newState = newstudents.map(obj => {
          // 👇️ if id equals 2, update country property
          if (obj.id === parseInt(getInfo.map(({id}) => id))) {
            return {...obj, stud_firstname: firstname,
              stud_lastname: lastname,
              stud_middlename: middlename,
              stud_address: address,
              stud_age: age,
              stud_zip: zip,
              stud_city: city,
              stud_regionProvince: regionprovince,
              stud_phoneNo: phone,
              stud_mobileNo: mobile,
              stud_email: email,
              stud_yrlvl: yrlevel,
              stud_section: section,
              sec_UpdatedAt: date.getUTCFullYear() + '-' +
              ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
              ('00' + date.getUTCDate()).slice(-2) + ' ' + 
              ('00' + date.getUTCHours()).slice(-2) + ':' + 
              ('00' + date.getUTCMinutes()).slice(-2) + ':' + 
              ('00' + date.getUTCSeconds()).slice(-2)};
          }
    
          // 👇️ otherwise return object as is
          return obj;
        });
        setNewStudents(newState);
        handleClose();
      
    };



    // functions for onchange.
    const handleFirstname = (e) => {
        setfirstName(e.target.value);
        // console.log(e.target.value);
    }
    const handleLastname = (e) => {
        setlastName(e.target.value);
        // console.log(e.target.value);
    }
    const handleMiddlename = (e) => {
        setmiddleName(e.target.value);
        // console.log(e.target.value);
    }
    const handleAddress = (e) => {
        setAddress(e.target.value);
        // console.log(e.target.value);
    }
    const handleAge= (e) => {
        setAge(e.target.value);
        // console.log(e.target.value);
    }
    const handleZIP = (e) => {
        setZIP(e.target.value);
        // console.log(e.target.value);
    }
    const handleCity = (e) => {
        setCity(e.target.value);
        // console.log(e.target.value);
    }
    const handleRegionProvince = (e) => {
        setRegionProvince(e.target.value);
        // console.log(e.target.value);
    }
    const handlePhone = (e) => {
        setPhone(e.target.value);
        // console.log(e.target.value);
    }
    const handleMobile = (e) => {
        setMobile(e.target.value);
        // console.log(e.target.value);
    }
    const handleEmail = (e) => {
        setEmail(e.target.value);
        // console.log(e.target.value);
    }
    const handleYrlevel = (e) => {
        setYrLevel(e.target.value);
        // console.log(e.target.value);
    }
    const handleSection = (e) => {
        setSection(e.target.value);
        // console.log(e.target.value);
    }

        // handle submt button.
        const handleSubmitBtn = (e) => {
          if((firstname === "" || lastname === "" || middlename === "" || address === "" || age === "" || zip === "" || city === "" || regionprovince === "" || phone === "" || mobile === "" || email === "" || yrlevel === "" || section === "") && e === true) {
              return true;
          } else {
              return false;
          }
      }

    return (
       <main>
        {searchEn &&
       <Form>
          <Table striped bordered hover size="sm" responsive="sm">
          <thead>
            <tr>
            <th>ID</th>
                    <th>Firstname</th>
                    <th>Lastsname</th>
                    <th>Middlename</th>
                    <th>Address</th>
                    <th>Age</th>
                    <th>Zip</th>
                    <th>City</th>
                    <th>Region</th>
                    <th>Phone Number</th>
                    <th>Mobile Number</th>
                    <th>Email</th>
                    <th>Year Level</th>
                    <th>Section</th>
                    <th>Updated</th>
                    <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {newstudents.filter(({id, stud_lastname, stud_firstname, stud_age}) => search2 !== '' ? stud_age >= parseInt(search) && stud_age <= parseInt(search2) : id === parseInt(search) || stud_lastname.toLowerCase() === search.toLowerCase() || stud_firstname.toLowerCase() === search.toLowerCase()).map((Student) => {
              return (
                <tr key={Student.id}>
                  <td>{Student.id}</td>
                    <td>{Student.stud_firstname}</td>
                    <td>{Student.stud_lastname}</td>
                    <td>{Student.stud_middlename}</td>
                    <td>{Student.stud_address}</td>
                    <td>{Student.stud_age}</td>
                    <td>{Student.stud_zip}</td>
                    <td>{Student.stud_city}</td>
                    <td>{Student.stud_regionProvince}</td>
                    <td>{Student.stud_phoneNo}</td>
                    <td>{Student.stud_mobileNo}</td>
                    <td>{Student.stud_email}</td>
                    <td>{Student.stud_yrlvl}</td>
                    <td>{Student.stud_section}</td>
                    <td>{Student.sec_UpdatedAt}</td>
                    <td>
                      <Button className="view-btn" variant="primary" type='submit' value={Student.id} onClick={handleClickModal}>
			                  View
		                  </Button>
                      <Button className="delete-btn" variant="danger" type='submit' value={Student.id} onClick={handleDelete}>
			                  Delete
		                  </Button>
                      <Button className="update-btn" variant="success" type='submit' value={Student.id} onClick={handleShowModalUpdate}>
			                  Update
		                  </Button>
                    </td>
                </tr>
              )
            })}

            {/* {newstudents.map((item) => (
                <tr key={item.id}>
                {Object.values(item).map((val) => (
                    <td key={val}>{val}</td>
                ))}

                <Button className="view-btn" variant="primary" type='submit' value={Student.id} onClick={handleClickModal}>
			                  View
		                  </Button>
                      <Button className="delete-btn" variant="danger" type='submit' value={Student.id} onClick={handleDelete}>
			                  Delete
		                  </Button>
                      <Button className="update-btn" variant="success" type='submit' value={Student.id} onClick={handleShowModalUpdate}>
			                  Update
		                  </Button>
                 </tr>
                 ))} */}

            </tbody>
          </Table>
          </Form>
        }
          {/* {showingModal && <ModalView data={studId} showM={true}></ModalView>}
         */}

        <div>
            {/* for adding student. */}
            {/* <Button variant="primary" onClick={handleShow}>Add Student</Button> */}
            <Modal show={show} onHide={handleHide}>
                <Modal.Header closeButton>Student Deatails</Modal.Header>
                <Modal.Body>
                    <Form onSubmit={handleUpdate}>
                    <Form.Group className="mb-3" controlId="formFirstname">
                        <Form.Label>First name
                        </Form.Label>
                        <Form.Control name="fname" type="text" value={firstname} onChange={handleFirstname} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formLastname" >
                        <Form.Label>Last name</Form.Label>
                        <Form.Control name="lname" type="text" placeholder="Enter last name" value={lastname} onChange={handleLastname} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formMiddlename">
                        <Form.Label>Middle name</Form.Label>
                        <Form.Control name="mname" type="text" placeholder="Enter middle name" value={middlename} onChange={handleMiddlename} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formAddress">
                        <Form.Label>Address</Form.Label>
                        <Form.Control name="address" type="text" placeholder="Address" value={address} onChange={handleAddress} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formAge">
                        <Form.Label>Age</Form.Label>
                        <Form.Control name="age" type="text" placeholder="Age" value={age} onChange={handleAge} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formZIP">
                        <Form.Label>ZIP</Form.Label>
                        <Form.Control name="zip" type="text" placeholder="ZIP code" value={zip} onChange={handleZIP} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formCity">
                        <Form.Label>City</Form.Label>
                        <Form.Control name="city" type="text" placeholder="City" value={city} onChange={handleCity} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formRegionProvince">
                        <Form.Label>Region or Province</Form.Label>
                        <Form.Control name="regionprovince" type="text" placeholder="Region/Province" value={regionprovince} onChange={handleRegionProvince} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formPhone">
                        <Form.Label>Phone no.</Form.Label>
                        <Form.Control name="phone" type="text" placeholder="Phone no." value={phone} onChange={handlePhone} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formMobile">
                        <Form.Label>Mobile no.</Form.Label>
                        <Form.Control name="mobile" type="text" placeholder="Mobile no." value={mobile} onChange={handleMobile} required disabled={!updateView}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control name="email" type="email" placeholder="Email" value={email} onChange={handleEmail} required disabled={!updateView}/>
                    </Form.Group>
                     <Form.Group className="mb-3" controlId="formYrLevel">
                        <Form.Label>Year Level</Form.Label>
                        <Form.Select name="yrlevel" value={yrlevel} aria-label="Default select example" onChange={handleYrlevel} required disabled={!updateView}>
                        <option>Year Level...</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formSection">
                      <Form.Label>Section</Form.Label>
                      <Form.Select name="section" value={section} aria-label="Default select example" onChange={handleSection} required disabled={!updateView}>
                        <option value="1">Saint Francis</option>
                        <option value="2">Saint Paul</option>
                        <option value="3">Saint Therese</option>
                        <option value="4">Saint John</option>
                        <option value="5">Saint Peter</option>
                        <option value="6">Saint Luke</option>
                      </Form.Select>
                    </Form.Group> 
                    <Modal.Footer>
                    <Button variant="danger" type="submit" onClick={handleClose}>
                        Cancel
                    </Button>
                    {updateView && <Button variant="primary" type="submit" disabled={handleSubmitBtn(true)} onClick={handleUpdate}>
                        Submit
                    </Button>}
                    </Modal.Footer>
                    </Form>
                </Modal.Body>
            </Modal>
        </div>
        </main>
    );
  }
  
  export default Display;